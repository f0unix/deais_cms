-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 18, 2017 at 01:36 PM
-- Server version: 5.7.17-0ubuntu0.16.04.1
-- PHP Version: 7.1.1-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `anr_deais`
--

-- --------------------------------------------------------

--
-- Table structure for table `Partnenaires`
--

CREATE TABLE `Partnenaires` (
  `partner_id` int(10) NOT NULL,
  `partenaire_nom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `partenaire_lien` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `partenaire_logo` blob,
  `partenaire_description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `Publications`
--

CREATE TABLE `Publications` (
  `publication_id` int(100) NOT NULL,
  `publication_lien` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `publication_description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `Reference`
--

CREATE TABLE `Reference` (
  `reference_id` int(100) NOT NULL,
  `reference_nom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `reference_description` text COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `Sections`
--

CREATE TABLE `Sections` (
  `section_id` int(11) NOT NULL,
  `section_nom` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `section_description` text COLLATE utf8_bin,
  `section_image` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Partnenaires`
--
ALTER TABLE `Partnenaires`
  ADD PRIMARY KEY (`partner_id`);

--
-- Indexes for table `Publications`
--
ALTER TABLE `Publications`
  ADD PRIMARY KEY (`publication_id`);

--
-- Indexes for table `Reference`
--
ALTER TABLE `Reference`
  ADD PRIMARY KEY (`reference_id`);

--
-- Indexes for table `Sections`
--
ALTER TABLE `Sections`
  ADD PRIMARY KEY (`section_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Partnenaires`
--
ALTER TABLE `Partnenaires`
  MODIFY `partner_id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Publications`
--
ALTER TABLE `Publications`
  MODIFY `publication_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Reference`
--
ALTER TABLE `Reference`
  MODIFY `reference_id` int(100) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Sections`
--
ALTER TABLE `Sections`
  MODIFY `section_id` int(11) NOT NULL AUTO_INCREMENT;