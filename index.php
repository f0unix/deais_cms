<?php

require "vendor/autoload.php";
use CrudKit\CrudKitApp;
use CrudKit\Pages\BasicLoginPage;
use Crudkit\Pages\HtmlPage;
use CrudKit\Pages\MySQLTablePage;

// Create a new CrudKitApp object
$app = new CrudKitApp();
$app->setStaticRoot ("static/crudkit/");
$app->setAppName ("DéAIS ANR Administrateur");

// 
// HANDLE LOGIN
// 

$login = new BasicLoginPage();

$login->setWelcomeMessage ("Utilisez vos credentiel pour se connecter");
if ($login->userTriedLogin ()) {
    $username = $login->getUserName ();
    $password = $login->getPassword ();

    // TODO: you should use your own authentication scheme here
    if ($username === 'admin' && $password === 'demo') {
        $login->success ();
    }
    else if ($username === 'user' && $password === 'demo') {
        $login->success ();
    }
    else {
        $login->fail ("Please check your password (admin/demo) or (user/demo)");
    }
}
$app->useLogin ($login);
// 
// END HANDLING LOGIN.
// 

if ($login->getLoggedInUser () === "user") {
    // If the user isn't `admin` then use read-only 
    $app->setReadOnly (true);
}

$partenaires = new MySQLTablePage ("Partenaires", "homestead", "secret", "anr_deais"); // Set a unique PAge ID too

$partenaires->setName("Partenaires")
    ->setTableName("Partenaires")
    ->setRowsPerPage (20)
    ->setPrimaryColumn("partenaire_id")
    ->addColumn("partenaire_nom", "Nom Partenaire", array(
        'required' => true
    ))
    ->addColumn("partenaire_lien", "Lien Web Partenaire", array(
        'required' => true
    ))
    ->addColumn("partenaire_logo", "Partenaire Logo", array(
        'required' => true
    ))
    ->addColumn('partenaire_description', "Description Partenaire", array(
        'required' => true
    ))
    ->setSummaryColumns(array("partenaire_nom"));


$app->addPage($partenaires);

/*
$invoice = new SQLiteTablePage ("sqlite1", "demo_database.sqlite");
$invoice->setName("Invoice")
    ->setPrimaryColumnWithId("a0", "InvoiceId")
    ->setTableName("Invoice")
    ->addColumnWithId("a1", "BillingCity", "City")
    ->addColumnWithId("a2", "BillingCountry", "Country")
    ->addColumnWithId("a3", "Total", "Total")
    ->addColumnWithId("a4", "InvoiceDate", "Date", array(
    ))
    ->setSummaryColumns(["a1", "a2", "a3", "a4"]);
$app->addPage($invoice);
*/
// Render the app. This will display the HTML
$app->render ();
